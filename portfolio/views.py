from django.http import JsonResponse
from django.shortcuts import render
from stocks.models import Stock, StockHistoric
import datetime

# Create your views here.
def home(request):
    return render(request, 'portfolio/home.html')


def get_stocks(request, exchange):
    return JsonResponse(list(set(Stock.objects.filter(exchange__code=exchange.upper()).values_list('code', flat=True))),
                        safe=False)


def get_stock_time_series(request, exchange, symb):
    try:
        if request.method == 'GET':
            pd = request.GET.get('period', '1y')
            filters = {}
            if pd == '1m':
                filters['r_date__gt'] = (datetime.datetime.now() - datetime.timedelta(days=30)).date()
            elif pd == '3m':
                filters['r_date__gt'] = (datetime.datetime.now() - datetime.timedelta(days=92)).date()
            elif pd == '6m':
                filters['r_date__gt'] = (datetime.datetime.now() - datetime.timedelta(days=183)).date()
            elif pd == '5d':
                filters['r_date__gt'] = (datetime.datetime.now() - datetime.timedelta(days=5)).date()
            elif pd == 'max':
                pass
            else:
                filters['r_date__gt'] = (datetime.datetime.now() - datetime.timedelta(days=365)).date()
            filters['stock'] = Stock.objects.get(exchange__code=exchange.upper(), code__iexact=symb)
            return JsonResponse(
                list(StockHistoric.objects.filter(**filters).order_by('r_date').values_list('r_date', 'price')), safe=False)
    except Exception as e:
        return JsonResponse({'error': str(e)})
    return JsonResponse({})