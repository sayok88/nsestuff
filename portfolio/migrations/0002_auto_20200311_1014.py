# Generated by Django 3.0.4 on 2020-03-11 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolio',
            name='sold_quantity',
            field=models.FloatField(default=0),
        ),
    ]
