from django.db import models
from django.contrib.auth.models import User
from stocks.models import Stock
from djmoney.models.fields import MoneyField


class Portfolio(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    stock = models.ForeignKey(Stock, on_delete=models.DO_NOTHING)
    bought_at = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    bought_quantity = models.FloatField()
    sold_at = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    sold_quantity = models.FloatField(default=0)


class Watchlist(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    stock = models.ForeignKey(Stock, on_delete=models.DO_NOTHING)
    auto_trigger = models.BooleanField(default=True)
    # TODO: more triggers and analysis configs
