import celery
import requests
from bs4 import BeautifulSoup

from stocks.models import Stock, Exchange, StockHistoric
from portfolio.models import Portfolio, Watchlist
import nsetools
import datetime

nse = nsetools.Nse()
nse_quote_fields = {'lastPrice': 'ltp', 'low52': 'l_52', 'high52': 'h_52', 'previousClose': 'p_close'}


@celery.task
def update_nse_stock(*args, **kwargs):
    code = kwargs['code']
    try:
        print(code)
        quote = nse.get_quote(str(code).lower())
        # print(quote)
        if quote:  # and all(name in nse_quote_fields.keys() for name in quote):
            stock = Stock.objects.get(code__iexact=code, exchange__code='NSE')
            for k in nse_quote_fields:
                setattr(stock, nse_quote_fields[k], quote[k])
            stock.last_updated = datetime.datetime.now()
            stock.save()

    except Exception as e:
        stock = Stock.objects.get(code__iexact=code, exchange__code='NSE')
        stock.error_raised = True
        stock.last_error = str(e)
        stock.save()


@celery.task
def refresh_stock_price(*args, **kwargs):
    stock1 = Portfolio.objects.filter(stock__exchange__code='NSE').distinct('stock')
    stock2 = Watchlist.objects.filter(stock__exchange__code='NSE').distinct('stock')
    stocks = stock1 | stock2
    for stock in stocks:
        if not stock.error_raised:
            update_nse_stock.delay(code=stock.code)


@celery.task
def first_nse_load(*args, **kwargs):
    all_stock_codes = nse.get_stock_codes()
    exc, _ = Exchange.objects.get_or_create(code='NSE', name='National Stock Exchange India')
    for stk in all_stock_codes:
        stock, _ = Stock.objects.get_or_create(code=stk, exchange=exc)
        stock.last_updated = datetime.datetime.now()
        stock.name = all_stock_codes[stk]
        stock.save()
        if not stock.error_raised:
            update_nse_stock.delay(code=stock.code)


@celery.task
def load_historic_data_by_date(*args, **kwargs):
    dt = kwargs['date'].strip()
    # print(dt)
    res = nse.download_bhavcopy(dt)
    data = res.decode('utf-8').split('\n')
    header = data[0].split(',')
    rows = [d.split(',') for d in data[1:]]
    dict_rows = []
    for row in rows:
        dict_rows.append({header[i]: row[i] for i in range(len(row))})
    blk = []
    for row in dict_rows:
        try:
            # insert_historic_price.delay(date=dt, code=row['SYMBOL'], price=row['CLOSE'])
            date = datetime.datetime.strptime(dt, '%Y-%m-%d')
            # print(date.date())
            price = eval(row['CLOSE'])
            code = row['SYMBOL']
            stock = Stock.objects.get(code__iexact=code, exchange__code='NSE')
            blk.append(StockHistoric(stock=stock, r_date=date.date(), price=price))
        except Exception as e:

            # print(row)
            # print(e)
            pass
    try:
        StockHistoric.objects.bulk_create(blk,500)
    except Exception as e:
        raise e
        pass
# @celery.task
# def insert_historic_price(*args, **kwargs):
#     date = datetime.datetime.strptime( kwargs['date'],'%Y-%m-%d')
#     price = eval(kwargs['price'])
#     code = kwargs['code']
#     stock = Stock.objects.get(code__iexact=code, exchange__code='NSE')
#     StockHistoric(stock=stock, r_date=date, price=price).save()


@celery.task
def load_cat_subcats(*args,**kwargs):
    base_url = 'https://www.moneycontrol.com'
    mcurl= 'https://www.moneycontrol.com/india/stockmarket/sector-classification/marketstatistics/nse/automotive.html'
    serch_url = 'india/stockmarket/sector-classification/marketstatistics/nse/'
    wiki_article_r = requests.get(mcurl)
    wiki_article_page = BeautifulSoup(wiki_article_r.content, features="html.parser")
    links = wiki_article_page.find_all('a')
    sector_urls = []
    table_class ='tbldata14 bdrtpg'
    # print(links)
    for l in links :
        # if hasattr(l,'href'):
        href = l.get('href')
        if href and serch_url in href:
            sector_urls.append({l.text:f'{base_url}{href}'})

    print(sector_urls)
    for l in sector_urls:
        wiki_article_r = requests.get(list(l.values())[0])
        wiki_article_page = BeautifulSoup(wiki_article_r.content, features="html.parser")
        table = wiki_article_page.find_all('table', {'class': table_class})
        if  table:
            trs = table[0].find_all('tr')

            for tr in trs:
                try:
                    print(f'{tr.find_all("td")[0].text.strip()}  {tr.find_all("td")[1].text.strip()}')
                except:
                    pass
                # wiki_data[tr.find_all('th')[0].text.strip()] = tr.find_all('td')[0].text.strip()
