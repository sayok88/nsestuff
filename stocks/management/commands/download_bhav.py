import csv
import datetime
import os
from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from portfolio.tasks import load_historic_data_by_date

class Command(BaseCommand):
    help = "Insert Upazila office reports from a CSV file. " \
           "CSV file name(s) should be passed. " \
           "If no optional argument (e.g.: --acland) is passed, " \
           "this command will insert UNO office reports."

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def add_arguments(self, parser):
        parser.add_argument('start_date',
                            nargs='+',
                            type=str,
                            help="14th Dec")
        parser.add_argument('end_date',
                            nargs='+',
                            type=str,
                            help="14th Dec")
        # Named (optional) arguments


    def handle(self, *args, **options):
        print(options)
        sdt = datetime.datetime.strptime( options['start_date'][0].strip(),'%Y-%m-%d')
        edt = datetime.datetime.strptime(options['end_date'][0].strip(),'%Y-%m-%d')
        diff_days = (edt -sdt).days
        for d in range(diff_days):
            dte = sdt+datetime.timedelta(days=d)
            load_historic_data_by_date.delay(date=dte.strftime('%Y-%m-%d'))
