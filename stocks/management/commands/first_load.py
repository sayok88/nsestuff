import csv
import datetime
import os
from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from portfolio.tasks import load_historic_data_by_date, first_nse_load


class Command(BaseCommand):
    help = "Insert Upazila office reports from a CSV file. " \
           "CSV file name(s) should be passed. " \
           "If no optional argument (e.g.: --acland) is passed, " \
           "this command will insert UNO office reports."

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def handle(self, *args, **options):
        first_nse_load.delay()
