from django.db import models
from djmoney.models.fields import MoneyField
import datetime


class Exchange(models.Model):
    code = models.CharField(primary_key=True, max_length=100)
    name = models.CharField(help_text='Full name of exchange',max_length=1000)

    def __str__(self):
        return self.code

class Stock(models.Model):
    exchange = models.ForeignKey(Exchange, on_delete=models.DO_NOTHING)
    code = models.CharField(null=False,max_length=100)
    name = models.CharField(max_length=1000)
    last_updated = models.DateTimeField(null=True)
    ltp = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    h_52 = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    l_52 = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    p_close = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    error_raised = models.BooleanField(default=False)
    last_error = models.CharField(max_length=3000, null=True)
    class Meta:
        unique_together = (('exchange', 'code'),)

    def __str__(self):
        return f'{self.exchange}:{self.code}'


class StockDaily(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.DO_NOTHING)
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)
    ptime = models.DateTimeField(auto_now_add=True)


class StockHistoric(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.DO_NOTHING)
    r_date = models.DateField(blank=True)
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null=True)

    # class Meta:
    #     unique_together = (('stock', 'r_date'),)


from .signals import *