from django.contrib import admin
from .models import *
# Register your models here.
class StockAdmin(admin.ModelAdmin):
    list_display = ('name1','ltp','h_52','l_52','p_close','error_raised')
    def name1(self,obj):
        return str(obj)

class HistoryAdmin(admin.ModelAdmin):
    list_display = ('stock','r_date','price')
admin.site.register(Stock, StockAdmin)
admin.site.register(Exchange)
admin.site.register(StockDaily)
admin.site.register(StockHistoric, HistoryAdmin)