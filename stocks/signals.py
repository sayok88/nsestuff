from django.db.models.signals import post_save
from django.dispatch import receiver

from stocks.models import Stock, StockDaily


@receiver(post_save, sender=Stock)
def stock_update_sig(sender, instance, created, **kwargs):
    if not created:
        StockDaily(stock=instance,price=instance.ltp).save()